let x = 1;
 
if (x === 1) {
  let x = 2;
 
  console.log(x);
  // expected output: 2
}
 
console.log(x); // 1 

// const number = 42;
// number = 100; // Uncaught TypeError: Assignment to constant variable.



// Rest Parameters
 
//first example
let scores = ['98', '95', '93', '90', '87', '85']
let [first, second, third, ...restOfScores] = scores;
 
console.log(first) // 98
console.log(second) // 95
console.log(third) // 93
console.log(restOfScores) // [90, 87, 85] 

//second example 
const filter = (...rest) =>{
    return rest.filter(el => el.text !== undefined)
}

console.log(filter(1, {text: "wonderful"}, "next"))

// spread operator
let array1 = ['one', 'two']
let array2 = ['three', 'four']
let array3 = ['five', 'six']
 
// ES5 Way / Normal Javascript
 
// var combinedArray = array1.concat(array2).concat(array3)
// console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']
 
// ES6 Way 
 
let combinedArray = [...array1, ...array2, ...array3]
console.log(combinedArray) // ['one', 'two', 'three', 'four', 'five', 'six']