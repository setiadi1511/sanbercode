
// soal 1
console.log("");
console.log('soal 1 :');
let jari = 5;
const luasLingkaran = (r) => {
    return 3.14 * r * r;
}
const kelilingLingkaran = (r) => {
    return 2 * 3.14 * r;
}

console.log(luasLingkaran(jari));
console.log(kelilingLingkaran(jari)); 
console.log("===================================");


// soal 2
console.log("");
console.log('soal 2 :');
let kalimat = "";
const tambahKata = (arr) => {
    return `${arr[0]} ${arr[1]} ${arr[2]} ${arr[3]} ${arr[4]}`;
}

console.log(tambahKata(['saya','adalah','seorang','frontend','developer']));
console.log("===================================");


// soal 3
console.log("");
console.log('soal 3 :');
const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            return `${firstName} ${lastName}`;
        }
    }
}

console.log(newFunction("William", "Imoh").fullName());
console.log("===================================");


// soal 4
console.log("");
console.log('soal 4 :');
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const {firstName,lastName,destination,occupation,spell} = newObject;
console.log(firstName, lastName, destination, occupation);
console.log("===================================");


// soal 5
console.log("");
console.log('soal 5 :');
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)
const combined = [...west, ...east];
console.log(combined)
console.log("===================================");