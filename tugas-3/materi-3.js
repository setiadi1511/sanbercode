var sayHello = "Hello World!" 
console.log(sayHello)

var name = "John" // Tipe
var angka = 12
var todayIsFriday = false 

console.log(name) // "John"
console.log(angka) // 12
console.log(todayIsFriday) // false

var sentences = "Javascript" 
console.log(sentences[0]) // "J"
console.log(sentences[2]) // "v"

var word = "Javascript is awesome"
console.log(word.length) // 21 

console.log('i am a string'.charAt(3)); // 'm'

var string1 = 'good';
var string2 = 'luck';
console.log(string1.concat(string2)); // goodluck

var text = 'dung dung ces!';
console.log(text.indexOf('dung'));  // 0
console.log(text.indexOf('u'));     // 1
console.log(text.indexOf('jreng')); // -1

var car1 = 'Lykan Hypersport';
var car2 = car1.substr(6);
console.log(car2); // Hypersport


var motor1 = 'zelda motor';
var motor2 = motor1.substr(2, 2);
console.log(motor2); // ld


var letter = 'This Letter Is For You';
var upper  = letter.toUpperCase();
console.log(upper); // THIS LETTER IS FOR YOU


var letter = 'This Letter Is For You';
var lower  = letter.toLowerCase();
console.log(lower); // this letter is for you


var username    = ' administrator ';
var newUsername = username.trim(); 
console.log(newUsername) // 'administrator'


var int  = 12;
var real = 3.45;
var arr  = [6, 7, 8];

var strInt  = String(int);
var strReal = String(real);
var strArr  = String(arr);

console.log(strInt);  // '12'
console.log(strReal); // '3.45'
console.log(strArr);  // '6,7,8'