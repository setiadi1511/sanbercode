
// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

kataKedua = kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1);

console.log("");
console.log('soal 1 :');
console.log(kataPertama + " " + kataKedua + " " + kataKetiga + " " + kataKeempat.toLocaleUpperCase());
console.log("===================================");


// soal2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

console.log("");
console.log('soal 2 :');
console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga)+parseInt(kataKeempat));
console.log("===================================");


// soal3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log("");
console.log('soal 3 :');
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);
console.log("===================================");


// soal4
var nilai = 95;

console.log("");
console.log('soal 4 :');
console.log("nilai :" + nilai);
if(nilai >= 80){
    console.log('A');
} else if(nilai >= 70 && nilai < 80){
    console.log('B');
} else if(nilai >= 60 && nilai < 70){
    console.log('C');
} else if(nilai >= 50 && nilai < 60){
    console.log('D');
} else {
    console.log('E');
}
console.log("===================================");


// soal5
var tanggal = 15;
var bulan = 11;
var tahun = 1984;

var bulanName = "";

switch(bulan) {
  case 1: {bulanName = "Januari"; break;}
  case 2: {bulanName = "Februari"; break;}
  case 3: {bulanName = "Maret"; break;}
  case 4: {bulanName = "April"; break;}
  case 5: {bulanName = "Mei"; break;}
  case 6: {bulanName = "Juni"; break;}
  case 7: {bulanName = "Juli"; break;}
  case 8: {bulanName = "Agustus"; break;}
  case 9: {bulanName = "September"; break;}
  case 10: {bulanName = "Oktober"; break;}
  case 11: {bulanName = "November"; break;}
default:  { bulanName = "Desember"; }}


console.log("");
console.log('soal 5 :');
console.log(tanggal + " " + bulanName + " " + tahun);
console.log("===================================");
