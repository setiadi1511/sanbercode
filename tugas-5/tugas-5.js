
// soal 1
console.log("");
console.log('soal 1 :');
function halo(){
    return "Halo Sanbers!"
}

console.log(halo()); // "Halo Sanbers!" 
console.log("===================================");


// soal 2
console.log("");
console.log('soal 2 :');
function kalikan(num1,num2){
    return num1 * num2;
}

var num1 = 12
var num2 = 4
 
var hasilKali = kalikan(num1, num2)
console.log(hasilKali) // 48
console.log("===================================");


// soal 3
console.log("");
console.log('soal 3 :');
function introduce(name, age, address, hobby){
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
}

var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di jalan belum jadi, dan saya punya hobby yaitu Gaming!" 
console.log("===================================");


// soal 4
console.log("");
console.log('soal 4 :');

var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var keyArr = ["nama", "jk", "hobi", "tahun"];
var objectDaftarPeserta = {};
arrayDaftarPeserta.forEach(function(item, index){
    objectDaftarPeserta[keyArr[index]] = item;
})
console.log(objectDaftarPeserta);
console.log("===================================");


// soal 5
console.log("");
console.log('soal 5 :');
var dataBuah = [
    {"nama":"strawberry","warna":"merah","ada bijinya":"tidak","harga":9000},
    {"nama":"jeruk","warna":"oranye","ada bijinya":"ada","harga":8000},
    {"nama":"Semangka","warna":"Hijau & Merah","ada ada":"tidak","harga":10000},
    {"nama":"Pisang","warna":"Kuning","ada bijinya":"tidak","harga":5000}
];
console.log(dataBuah[0]);
console.log("===================================");



// soal 6
console.log("");
console.log('soal 6 :');
var dataFilm = []

dataFilm.push({nama:"Mulan", durasi:"2:14" , genre:"action", tahun:"2019"})
dataFilm.push({nama:"Fast & Furious 9", durasi:"1:44" , genre:"action", tahun:"2020"})
dataFilm.push({nama:"The Way Back", durasi:"2:04" , genre:"action", tahun:"2020"})
dataFilm.push({nama:"James Bond - No Time To Die ", durasi:"1:04" , genre:"action", tahun:"2021"})

console.log(dataFilm);
console.log("===================================");