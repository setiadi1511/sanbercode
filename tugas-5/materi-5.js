let book = {name: "biology", totalPage: 50}

console.log(book)


console.log("======================")
var pesertaLomba= [
    ["Budi", "Pria", "172cm"], 
    ["Susi", "Wanita", "162cm"], 
    ["Lala", "Wanita", "155cm"], 
    ["Agung", "Pria", "175cm"]
];

var resultPesertaLomba = [];
pesertaLomba.forEach((item, index) =>{
    resultPesertaLomba.push({
        "nama": item[0],
        "jk": item[1],
        "tinggi": item[2],
    })
});
console.log(resultPesertaLomba);
console.log("======================")


function luasLingkaran(jari){
    return 3.14*jari*jari;
}
function luasSegitiga(alas,tinggi){
    return 0.5*alas*tinggi;
}
function luasPersegi(panjang,lebar){
    return panjang*lebar;
}
console.log(luasLingkaran(5));
console.log(luasSegitiga(2,4));
console.log(luasPersegi(3,4));
console.log("======================")



var daftarAlatTulis = ["2. Pensil","5. Penghapus","3. Pulpen","4. Penggaris","1. Buku"];
daftarAlatTulis = daftarAlatTulis.sort();
var flag = 0;
while(flag < daftarAlatTulis.length) {
    console.log(daftarAlatTulis[flag]);
    flag++;
}
console.log("======================")




var daftarNama = []
function tambahNama(nama,jenisKelamin){
    daftarNama.push({
        nama: nama,
        jenisKelamin: jenisKelamin,
        panggilan: jenisKelamin == "laki-laki" ? "Bapak" : "Ibu"
    })
}

tambahNama("Asep","laki-laki");
tambahNama("Siti","perempuan");
tambahNama("Yeni","perempuan");
tambahNama("Rudi","laki-laki");
tambahNama("Adit","laki-laki");
daftarNama.forEach((item,index)=> {
    console.log(""+(index+1)+". "+item.panggilan+" "+item.nama+"");
})