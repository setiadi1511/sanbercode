var flag = 1;
while(flag < 10) { // Loop akan terus berjalan selama nilai flag masih dibawah 10
  console.log('Iterasi ke-' + flag); // Menampilkan nilai flag pada iterasi tertentu
  flag++; // Mengubah nilai flag dengan menambahkan 1
}

// output

// Iterasi ke-1
// Iterasi ke-2
// Iterasi ke-3
// Iterasi ke-4
// Iterasi ke-5
// Iterasi ke-6
// Iterasi ke-7
// Iterasi ke-8
// Iterasi ke-9


var deret = 5;
var jumlah = 0;
while(deret > 0) { // Loop akan terus berjalan selama nilai deret masih di atas 0
  jumlah += deret; // Menambahkan nilai variable jumlah dengan angka deret
  deret--; // Mengubah nilai deret dengan mengurangi 1
  console.log('Jumlah saat ini: ' + jumlah)
}
 
console.log(jumlah);

// output

// Jumlah saat ini: 5
// Jumlah saat ini: 9
// Jumlah saat ini: 12
// Jumlah saat ini: 14
// Jumlah saat ini: 15
// 15