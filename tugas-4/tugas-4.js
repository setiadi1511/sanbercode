
// soal 1
console.log("");
console.log('soal 1 :');
console.log('LOOPING PERTAMA');
var flag = 2;
while(flag <= 20) {
    console.log(flag + " - I love coding");
  flag+=2;
}

console.log('LOOPING KEDUA');
var flag = 20;
while(flag >= 2) {
    console.log(flag + " - I will become a frontend developer");
  flag-=2;
} 
console.log("===================================");



// soal 2
console.log("");
console.log('soal 2 :');
for(var deret = 1; deret <= 20; deret++) {
    if(deret %3 == 0 && deret %2 == 1){
        console.log(deret + " - I Love Coding");
    } else if(deret %2 == 0){
        console.log(deret + " - Berkualitas");
    } else {
        console.log(deret + " - Santai");
    }
}
console.log("===================================");


// soal 3
console.log("");
console.log('soal 3 :');
var cart = "";
for(var deret = 1; deret <= 7; deret++) {
    cart += "#";
    console.log(cart);
}
console.log("===================================");


// soal 4
console.log("");
console.log('soal 4 :');
var cart = "";
var kalimat="saya sangat senang belajar javascript"
var out = kalimat.split(" ")
console.log(out);
console.log("===================================");


// soal 5
console.log("");
console.log('soal 5 :');
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah = daftarBuah.sort();
for(var deret = 0; deret < daftarBuah.length; deret++) {
    console.log(daftarBuah[deret]);
}
console.log("===================================");