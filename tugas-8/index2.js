var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var timer = 10000;
var x = 0;
var loopArray = function(arr) {
    readBooksPromise(timer, arr[x])
        .then(function (callback) {
            x++;
            timer = callback;
            if(x < arr.length) {
                loopArray(arr);   
            }
        })
        .catch(function (error) {
            console.log(error.message);
        });
}
loopArray(books);